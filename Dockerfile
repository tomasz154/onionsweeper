# Stage 1

FROM node as builder

WORKDIR /build/

COPY package.json yarn.lock ./
RUN yarn

COPY . .
RUN yarn build

# Stage 2

FROM nginx

COPY --from=builder /build/build /usr/share/nginx/html
